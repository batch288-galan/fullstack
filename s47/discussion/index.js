// console.log('Hello!');

//Section: Document Object Model (DOM)
  //allows us to acces or modify the properties of an html element in a webpage
  //it is standard on how to get, change, add or delete HTML elements
  //we will be focusing only with DOM in  terms of managing forms
  
  //using the querySelector it can access/get the HTML element/s

    //CSS selectors to target specific element
      //id selector(#);
      //class selector(.);
      //tag/type selector(html tags);
      //universal selector(*);
      //attribute selector([attribute]);
  
  //Query selectors has two types: querySelector and querySelectorAll

// console.log(firstElement);

  //querySelector
  let secondElement = document.querySelector('.full-name');
  console.log(secondElement)
  //querySelectorAll
  let thirdElement = document.querySelectorAll('.full-name')
  ;
  console.log(thirdElement);


  //getElements
  let element = document.getElementById('fullName');
  console.log(element);

  element = document.getElementsByClassName("full-name");
  console.log(element);


// Section: Event Listeners
  //whenever a user interacts with a webpage, this action is considered as event
  //working with events is large part of creating interactivityin a webpage
  //specific function will be invoked if the event happen

  //funcion 'addEventListener', it takes two arguments
    //first argument a string identifying the event
    //second, argument function that the listerner will inboke once the specified event occur.


 /* let fullName = document.querySelector('#fullName');

  let firstElement = document.querySelector('#txt-first-name');

  let txtLastName = document.querySelector('#txt-last-name');

  firstElement.addEventListener('keyup', ()=>{
    console.log(firstElement.value);

    fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`
  })

  
  txtLastName.addEventListener('keyup', ()=>{
    console.log(txtLastName.value);

    fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`
  })*/


let firstNameInput = document.querySelector('#txt-first-name');
let lastNameInput = document.querySelector('#txt-last-name');
let fullName = document.querySelector('#fullName');
let textColorSelect = document.querySelector('#text-color');

firstNameInput.addEventListener('keyup', updateFullName);
lastNameInput.addEventListener('keyup', updateFullName);
textColorSelect.addEventListener('change', updateTextColor);

function updateFullName() {
   let firstName = firstNameInput.value;
   let lastName = lastNameInput.value;
   fullName.innerHTML = `${firstName} ${lastName}`;
 }

 function updateTextColor() {
   let selectedColor = textColorSelect.value;
   fullName.style.color = selectedColor;
 }