import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate, Link, useNavigate} from 'react-router-dom';

import Swal2 from 'sweetalert2';

import UserContext from '../UserContext.js';

export default function Login(){

	const [email, setEmail] = useState('');
	const [password, setPassword] =  useState('');
	const [isDisabled, setIsDisabled] = useState(true);
	// const [user, setUser] = useState(localStorage.getItem('email'));

	const {user, setUser} = useContext(UserContext);

	const navigate = useNavigate();

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/user-details`, {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data=> {
			console.log(data);
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {

		if (email !== '' && password !== '') {
			setIsDisabled(false);
		} else {
			setIsDisabled(true)
		}

	}, [email, password]) //dependencies

	const Login = (e) => {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			if (data === false) {
				
				Swal2.fire({
					title: 'Athentication Failed!',
					icon: 'error',
					text: 'Check you login details and try again!'
				})

			} else {
				
				localStorage.setItem('token', data.auth);
				retrieveUserDetails(data.auth);

				Swal2.fire({
					title: 'Login Successful!',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})

				navigate('/');
			}
		})

		/*alert('User Successfully Logged in!');

		localStorage.setItem('email', email)
		setUser(localStorage.getItem('email'))

		navigate('/');*/

		/*setEmail('');
		setPassword('');*/

	}

	return(
			
			<Container className = "mt-5">
				<Row>
					<Col className = "col-6 mx-auto">

						<h1 className = "text-center">Login</h1>
						<Form onSubmit = {event => Login(event)}>
						      <Form.Group className="mb-3" controlId="formBasicEmail">
						        <Form.Label>Email address</Form.Label>
						        <Form.Control value = {email} onChange = {event => setEmail(event.target.value)} type="email" placeholder="Enter email" />
						        
						      </Form.Group>

						      <Form.Group className="mb-3" controlId="formBasicPassword">
						        <Form.Label>Password</Form.Label>
						        <Form.Control value = {password} onChange = {event => setPassword(event.target.value)} type="password" placeholder="Password" />
						      </Form.Group>

						      <p>No Account Yet? <Link as = {Link} to = '/register'>Sign-Up Here</Link></p>
						      
						      <Button disabled = {isDisabled} variant="primary" type="submit">
						        Submit
						      </Button>
						    </Form>
					</Col>
				</Row>
			</Container> 

		)
}