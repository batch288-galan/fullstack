import CourseCard from '../components/CourseCard.js';

import {useState, useEffect} from 'react';

export default function Courses(){


	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active-courses`)
		.then(result => result.json())
		.then(data => {
			setCourses(data.map(course => {
				return(

							<CourseCard key = {course._id} courseProp = {course}/>

					  )
			}))
		})
	}, [])

	
	/*const courses = coursesData.map(course => {

		return(
			
					<CourseCard key = {course.id} courseProp = {course}/>
 
			  )
	}) */




		return(
		
					<>
						<h1 className = 'mt-3 text-center'>Courses</h1>
						{courses}
					</>

			  )
		
}