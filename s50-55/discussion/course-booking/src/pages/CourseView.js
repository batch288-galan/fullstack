import {useState, useEffect} from 'react';
import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom';
import Swal2 from 'sweetalert2';

export default function CourseView(){

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');

    const {courseId} = useParams();
    console.log(courseId);

    const navigate = useNavigate();

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
        .then(result => result.json())
        .then(data => {
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
        })

    }, [])

    const Enroll = (courseId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                id: courseId
            })
        })
        .then(result => result.json())
        .then(data => {
            // console.log(data)

            if(data) {
                Swal2.fire({
                    title: 'Successfully Enrolled',
                    icon: 'success'
                })

                navigate('/courses');

            } else {
                Swal2.fire({
                    title: 'Error, try again',
                    icon: 'error'
                })
            }

        })
    }

    return(
        <Container className="mt-5">
            <Row>
                <Col>

                    <Card> 
                          <Card.Body>
                            <Card.Title>{name}</Card.Title>
                            
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>

                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>{price}</Card.Text>

                            <Card.Subtitle>Class Schedule:</Card.Subtitle>
                            <Card.Text>8am - 5pm</Card.Text>

                            <Button onClick = {()=> Enroll(courseId) } variant="primary">Enroll!</Button>
                          </Card.Body>
                    </Card>

                </Col>
            </Row>
        </Container>
    )
}