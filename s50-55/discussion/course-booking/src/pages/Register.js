import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';

export default function Register(){

	const [firstName,setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('')
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [password2, setPassword2] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	const {setUser} = useContext(UserContext);

	const navigate = useNavigate();

	useEffect(() => {

		if (email !== '' && password !== '' && password2 !== '' && password === password2 && firstName !== '' && lastName !== '' && mobileNo !== '' && mobileNo.length > 10) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true)
		}

	}, [email, password, setPassword2, firstName, lastName, mobileNo]) //dependencies

	function Register(event) {
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
				method: 'POST',
				headers: {
					'Content-type': 'application/json'
				},
				withCredentials: true,
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					mobileNo: mobileNo,
					email: email,
					password: password
				})		
		})
		.then(result => result.json())
		.then(data => {
			if (data === false) {
				Swal2.fire({
					title: 'Error',
					icon: 'error',
					text: 'Error! Try again!'
				})
			} else {

				Swal2.fire({
					title: 'Success',
					icon: 'success',
					text: 'Registration Successful'
				})

				navigate('/login');
			}
		})

		/*localStorage.setItem('email', email);
		setUser(localStorage.getItem('email'));

		alert('Thank you for registering!');

		navigate('/');*/

		//clear input fields
		// setEmail('');
		// setPassword1('');
		// setPassword2('');
} 

	return(

		

			<Container className = "mt-5">
				<Row>
					<Col className = "col-6 mx-auto">
						<h1 className = "text-center">Register</h1>
						<Form onSubmit = {event => Register(event)}>

								  <Form.Group className="mb-3" controlId="formBasicFirst">
								    <Form.Label>First Name</Form.Label>
								    <Form.Control value = {firstName} onChange = {event => setFirstName(event.target.value)} type="text" />
								  </Form.Group>

								  <Form.Group className="mb-3" controlId="formBasicLast">
								    <Form.Label>Last Name</Form.Label>
								    <Form.Control value = {lastName} onChange = {event => setLastName(event.target.value)} type="text" />
								  </Form.Group>

								  <Form.Group className="mb-3" controlId="formBasicMobile">
								    <Form.Label>Mobile</Form.Label>
								    <Form.Control value = {mobileNo} onChange = {event => setMobileNo(event.target.value)} type="text" />
								  </Form.Group>

							      <Form.Group className="mb-3" controlId="formBasicEmail">
							        <Form.Label>Email address</Form.Label>
							        <Form.Control value = {email} onChange = {event => setEmail(event.target.value)} type="email" />
							        
							      </Form.Group>

							      <Form.Group className="mb-3" controlId="formBasicPassword">
							        <Form.Label>Password</Form.Label>
							        <Form.Control value = {password} onChange = {event => setPassword(event.target.value)} type="password" />
							      </Form.Group>

							      <Form.Group className="mb-3" controlId="formConfirmPassword">
							        <Form.Label>Confirm Password</Form.Label>
							        <Form.Control value = {password2} onChange = {event => setPassword2(event.target.value)} type="password" />
							      </Form.Group>

							      <p>Have An Account Already? <Link as = {Link} to = '/login'>Login Here</Link></p>
							      
							      <Button onClick={() =>Register} disabled = {isDisabled} variant="primary" type="submit">
							        Submit
							      </Button>
						    </Form>
					</Col>
				</Row>
			</Container>

		)
}