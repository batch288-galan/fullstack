import './App.css';

import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext.js';

import {Navigate} from 'react-router-dom';

/*Routes or pages*/
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import CourseView from './pages/CourseView.js';
import PageNotFound from './pages/PageNotFound.js';    

/*react-bootstrap minified css link*/
import 'bootstrap/dist/css/bootstrap.min.css';

/*Used for routing*/
import {BrowserRouter, Route, Routes} from 'react-router-dom';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {

    console.log(user);
    console.log(localStorage);

  }, [user])


  useEffect(() => {

    if (localStorage.getItem('token')) {
        fetch(`${process.env.REACT_APP_API_URL}/users/user-details`, {
          method: 'GET',
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then(result => result.json())
        .then(data=> {
          // console.log(data);
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        })
    }
  }, [])


  const Inaccessible = () => {
     return <Navigate to="/inaccessible" />;
   }


  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
        <BrowserRouter>
            <AppNavBar /> 
              
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/courses" element={<Courses />} />
                <Route path="/courses/:courseId" element={<CourseView />} />

                {user.id === null ? (
                  <>
                    <Route path="/register" element={<Register />} />
                    <Route path="/login" element={<Login />} />
                  </>
                ) : (
                  <>
                    <Route path="/register" element={<Inaccessible />} />
                    <Route path="/login" element={<Inaccessible />} />
                  </>
                )}

                <Route path="/logout" element={<Logout />} />
                <Route path="/inaccessible" element={<PageNotFound/>} />
                <Route path="*" element={<Inaccessible />} />
            </Routes>

        </BrowserRouter>
    </UserProvider>
  
  );
}

export default App;
