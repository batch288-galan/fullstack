import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext.js';

import {Link} from 'react-router-dom';

export default function CourseCard(props) {
/*
    console.log(props);
    console.log(props.courseProp);

    //object destructuring

    console.log(id);*/

    const {user} = useContext(UserContext);

    const {_id, name, description, price} = props.courseProp;

    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(30);
    const [isDisabled, setIsDisabled] = useState(false);
    

    function enroll() {

        if (seats > 0) {
            setCount(count + 1);
            setSeats(seats - 1);
        } else {
            alert('No more seats availablex!');
        }
        
      };

    useEffect(() => {
      // setIsDisabled(seats === 0);

        if (seats === 0) {
            setIsDisabled(true);
        }

    }, [seats]);

    return(
        <Container>
            <Row>
                <Col className="col-12 mt-3">
                    <Card>
                        <Card.Body>
                            <Card.Title className="mb-3 cardTitle">{name}</Card.Title>
                            <Card.Subtitle className="cardDescription">Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle className="cardPrice">Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>

                            <Card.Subtitle className="enrollees">Enrollees</Card.Subtitle>
                            <Card.Text> {count} </Card.Text>

                            <Card.Subtitle className="seats">Seats</Card.Subtitle>
                            <Card.Text> {seats} </Card.Text>

                            {
                                user !== null ?

                                <Button as = {Link} to = {`/courses/${_id}`}>Details</Button>

                                :

                                <Button as = {Link} to = '/login'>Login To Enroll</Button>
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}
